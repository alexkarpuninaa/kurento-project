import '../styles/globals.css';
import { SocketContextProvider } from '../src/context/SocketContext';

function MyApp({ Component, pageProps }) {
  return (
    <SocketContextProvider>
      <Component {...pageProps} />
    </SocketContextProvider>
  )
}

export default MyApp

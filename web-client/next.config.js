/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    SOCKET_URL: process.env.SOCKET_URL,
    ICE_SERVERS: [
      {
        url: "stun:stun3.l.google.com:19302",
      },
      {
        url: "turn:192.158.29.39:3478?transport=udp",
        credential: "JZEOEt2V3Qb0y27GRntt2u2PAYA=",
        username: "28224511:1379330808",
      },
    ],
  },
};

module.exports = nextConfig;

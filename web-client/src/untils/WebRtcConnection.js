import 'webrtc-adapter';

import getUserMedia from '../helpers/getUserMedia';
import getConstraints from '../helpers/getConstraints';

export class WebRtcConnection {
    constructor(data) {
        this.callId = data.callId;
        this.type = data.type;
        this.iceServers = data.iceServers;
        this.localStream = null;
    }

    createPeerConnection = () => {
        this.peerConnection = new RTCPeerConnection({
            iceServers: this.iceServers,
        });
    };

    generateLocalStream = async () => {
        const constraints = getConstraints();
        this.localStream = await getUserMedia(constraints);
    };

    createOffer = async () => {
        if (this.localStream) {
            this.peerConnection.addStream(
                new MediaStream(
                    this.localStream.getTracks()
                )
            );
        }

        const offer = await this.peerConnection.createOffer();

        await this.peerConnection.setLocalDescription(offer);
    
        return offer;
    };

};

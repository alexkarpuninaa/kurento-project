import React, { useCallback, useEffect, useRef, useState } from "react";
import { io } from 'socket.io-client';
import { WebRtcConnection } from "../untils/WebRtcConnection";

const SocketContext = React.createContext();

const SocketContextProvider = ({ children }) => {
    const socket = useRef(null);
    const [isConnectedToSocket, setIsConnectedToSocket] = useState(false);

    const webRtcConnection = new WebRtcConnection({
        callId: 'group-room',
        type: '',
        iceServers: process.env.ICE_SERVERS,
    });

    const init = () => {
        socket.current = io(process.env.SOCKET_URL, {
            transports: ['websocket', 'polling'],
        });

        socket.current.on('connect', () => {
            console.log('socket connected: ', socket.current.connected);
            setIsConnectedToSocket(true);
        });
    };

    const emitHandler = useCallback((event, data) => {
        if (socket.current) {
            socket.current.emit(event, data);
        }
    }, [socket]);

    const sendOffer = async () => {
        const offer = await webRtcConnection.createOffer();
        emitHandler('v1:video-chat:offer', {
            offer: offer.sdp,
            callId: webRtcConnection.callId,
        });
    }

    useEffect(() => {
        if (!socket.current) {
            init();
        }
    }, []);

    useEffect(() => {
        if (isConnectedToSocket) {
            webRtcConnection.generateLocalStream();
            webRtcConnection.createPeerConnection();
            sendOffer();
        }
    }, [isConnectedToSocket])

    return (
        <SocketContext.Provider
            value={{
                action: {},
                value: {},
            }}
        >
            {children}
        </SocketContext.Provider>
    )

};

export {
    SocketContext,
    SocketContextProvider,
}

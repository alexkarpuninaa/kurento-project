export default async function getUserMedia (constraints) {
    const copyConstraints = [...constraints];

    try {
        const constraint = copyConstraints.shift();
        return await navigator.mediaDevices.getUserMedia(constraint);
    } catch (e) {
        console.warn('getUserMedia error :', e);
        if (!copyConstraints.length) {
            throw e;
        }
        return getUserMedia(copyConstraints);
    }
};

const kurentoClient = require('kurento-client');
const { kurentoUrl } = require('../config/kurento-config');

class Kurento {
    constructor () {
        this.connection = null;
    }

    async getKurentoConnection () {
        if (this.connection) return this.connection;

        this.connection = await kurentoClient(kurentoUrl);
        return this.connection;
    };

    async createPipeline () {
        const connection = await this.getKurentoConnection();
        return connection.create('MediaPipeline');
    };

    async createWebRtcEndPoint (pipeline) {
        return pipeline.create('WebRtcEndpoint');
    };

};

module.exports = new Kurento();

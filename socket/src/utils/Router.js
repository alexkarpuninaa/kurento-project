class Router {
    constructor({
        path,
    }) {
        this.path = path;
        this.routes = [];
    };

    addRouter(path, options, handler) {
        this.routes.push([path, options, handler]);
    };

    addSocket(socketTransport, route) {
        route.routes.forEach(([route, _, handler]) => {
            socketTransport.addRoute(`${this.path}${route}`, (data) => {
                this.runHandler({
                    handler, socketTransport, data,
                });
            });
        });
    };

    async runHandler({
        handler, socketTransport, data,
    }) {
        try {
            await handler(socketTransport, data);
        } catch (e) {
            console.log(e);
            socketTransport.disconnect();
        }
    }

};

module.exports = Router;

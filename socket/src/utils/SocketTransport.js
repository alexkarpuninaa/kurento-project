class SocketTransport {
    constructor({
        io,
        socket,
        routes,
    }) {
        this.io = io;
        this.socket = socket;
        this.routes = routes;
        this.id = socket.id;

        this.init({ routes });
    };

    async init({ routes }) {
        routes.map((route) => route.addSocket(this, route))

        this.addRoute('disconnect', this.onDisconnect);
    };

    addRoute(event, handler) {
        this.socket.on(event, handler);
    };

    async onDisconnect () {
        console.log(`DISCONNECTED ${this.id}`);
    };
};

module.exports = SocketTransport;


const routes = require('../routes/index');
const SocketTransport = require('./SocketTransport');

const onConnection = (socket, io) => {
  new SocketTransport({ io, socket, routes });
};

const wrapWithCatch = (func, io) => async (socket) => {
  try {
    await func(socket, io);
  } catch (e) {
    socket.disconnect();
  }
};

module.exports = function initIo(io) {
  try {
    io.on('connection', wrapWithCatch(onConnection, io));
  } catch (e) {
    console.warn("initIO error: ", e);
  }
};

const VideoService = require("../services/VideoService");

const publish = async (socketTransport, data) => {
    const { offer, callId } = data;
    const response = await VideoService.publish(
        socketTransport, { offer, callId }
    );
    console.log('response', response);
}

module.exports = {
    publish,
};

const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);
const { PORT }  = require('./config/socket-config');
const initIo = require('./utils/initIo');

initIo(io);

server.listen(PORT, () => {
  console.log(`listening on *:${PORT}`);
});

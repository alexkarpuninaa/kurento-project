module.exports = {
    kurentoUrl: 'ws://localhost:8888/kurento',
    iceServers: {
        stun: {
            ip: '23.21.150.121',
            port: 3478,
        },
        turn: 'turn:numb.viagenie.ca',
    }
};

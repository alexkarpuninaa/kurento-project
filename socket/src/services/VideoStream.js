const { iceServers } = require('../config/kurento-config');

class VideoStream {
    constructor (data) {
        this.callId = data.callId;
        this.endPoint = data.endPoint;
    }

    processOffer = async (offer) => {
        return this.endPoint.processOffer(offer);
    }

    configureEndPoint = async () => {
        await this.endPoint.setStunServerAddress(iceServers.stun.ip);
        await this.endPoint.setStunServerPort(iceServers.stun.port);
        await this.endPoint.setTurnUrl(iceServers.turn);
    }
}

module.exports = VideoStream;

const Kurento = require('../utils/Kurento');
const VideoStream = require('./VideoStream');

class VideoService {
    constructor () {
        this.videoStreams = {};
    }

    publish = async (socketTransport, { offer, callId }) => {
        const { videoStream, answer} = await this.createUserStream(
            socketTransport, { offer, callId }
        );

        return { answer, callId}
    }

    createUserStream = async (socketTransport, data) => {
        const {callId, offer} = data;

        const pipeline = await Kurento.createPipeline();
        const endPoint = await Kurento.createWebRtcEndPoint(pipeline);

        const videoStream = new VideoStream({
            endPoint,
            callId,
        });

        await videoStream.configureEndPoint();

        this.videoStreams[callId] = videoStream;
        const answer = await videoStream.processOffer(offer);

        return { videoStream, answer };
    }

}

module.exports = new VideoService();

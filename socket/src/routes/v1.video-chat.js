const Router = require('../utils/Router');
const controller = require('../controllers/video-chat.controller');

const router = new Router({
    path: 'v1:video-chat:',
});

router.addRouter(
    'offer',
    {},
    controller.publish,
);

module.exports = router;

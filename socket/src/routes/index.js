const videoChatRoutes = require('./v1.video-chat');

const routes = [
    videoChatRoutes,
];

module.exports = routes;

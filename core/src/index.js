const express = require('express');
const { PORT } = require('./config/core-config');

const app = express();

app.listen(PORT, () => console.log(`Example app is listening on port ${PORT}.`));
